# pvaDriver conda recipe

Home: "https://github.com/areaDetector/pvaDriver"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS pv access driver for cameras
